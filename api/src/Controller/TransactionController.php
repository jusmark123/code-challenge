<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\TransactionService;

class TransactionController
{
    /**
     * @Route(
     *     name="api_transactions_do_transaction",
     *     path="/api/transaction/{total_cost}/{amount_provided}",
     *     methods={"GET","POST"},
     *     defaults={
     *      "_controller" = "\App\Controller\TransactionController::do_transaction",
     *     }
     * )
     */


    public function do_transaction(Request $data)
    {

        $transactionService = new TransactionService();

        $total_cost = $data->get('total_cost');
        $amount_provided = $data->get('amount_provided');

        $data = json_encode($transactionService->get_denom($total_cost, $amount_provided));

        return new Response($data);
    }

}
