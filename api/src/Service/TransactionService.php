<?php


namespace App\Service;

class TransactionService
{

    const DENOMS = [
        'hundreds' => 100,
        'fifties' => 50,
        'twenties' => 20,
        'tens' => 10,
        'fives' => 5,
        'ones' => 1,
        'quarters' => .25,
        'dimes' => .10,
        'nickles' => .05,
        'pennies' => .01];

    private $transact_array = array();

    /**
     * @param float $total_cost
     * @param float $amount_provided
     *
     * @return array|false|string
     */
    public function get_denom(float $total_cost, float $amount_provided)
    {
        try {

            if(!is_numeric($total_cost) && !is_numeric($amount_provided)) {
                throw new \Exception('Input values must be float or integer');
            }

            $change = (float) abs($amount_provided) - (float) abs($total_cost);

            $balance = $change;

            $this->transact_array = array_combine(array_keys(self::DENOMS),
                array_fill(0, count(self::DENOMS), 0));

            if ($amount_provided > $total_cost) {
                foreach (self::DENOMS as $key => $value) {
                    if ($balance > 1) {
                        $balance = $this->check_denom($balance, $value, $key);
                    } else {
                        $balance = $balance * 100;
                        $value = $value * 100;
                        $balance = $this->check_denom($balance, $value, $key);
                        $balance = $balance / 100;
                    }
                }

                $this->transact_array['change'] = number_format($change, 2);
                $this->transact_array['balance'] = number_format($balance, 2);
                $this->transact_array['message'] = "Calculation successful";
                $message = $this->transact_array;
            } else {
                $message = "Amount provided must be greater than total cost";
            }

        } catch(\Exception $e)
        {
            $message = $e->getMessage();
        }

        return $message;
    }

    /**
     * @param $balance
     * @param $value
     * @param $key
     * @return float|int
     */
    public function check_denom($balance, $value, $key)
    {
        $denom = (int)($balance/$value);

        if($denom > 0) {
            $this->transact_array[$key] = $denom;
            $balance -= $value * $denom;
        }

        return $balance;
    }
}