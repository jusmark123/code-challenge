<?php


namespace App\Tests;
use App\Service\TransactionService;
use PHPUnit\Framework\TestCase;

class DoTransactionTest extends TestCase
{
    public function testTransaction()
    {
        $total_cost = 20.00;
        $amount_provided = 50.00;

        $service = new TransactionService();

        $transact_array = $service->get_denom($total_cost, $amount_provided);

        $this->assertEquals(30.00, $transact_array['change']);
        $this->assertEquals(0.00, $transact_array['balance']);
        $this->assertEquals(1, $transact_array['tens']);
        $this->assertEquals(0, $transact_array['hundreds']);

    }
}