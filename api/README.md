## Code Challenge

This is a very basic api, I wasn't sure how far to go, and I didn't want to take took much time.
Please let me know how I can improve.

There is only one route

/api/transaction/{total_cost}/{amount_provided}

## Code

Transaction Controller - Calls Action controller<br>
**/api/src/controller/TransactionController.php**

Transaction Service - The Action controller handles actual calculation<br>
**/api/src/Controller/TransactionService.php**

Unit Test - Simple Unit Test<br>
**/api/tests/DoTransactionTest.php**

**Successful Response**<br>
Response will be JSON array
```javascript
{
  "hundreds": 0,
  "fifties": 0,
  "twenties": 0,
  "tens": 0,
  "fives": 0,
  "ones": 0,
  "quarters": 2,
  "dimes": 2,
  "nickles": 0,
  "pennies": 1,
  "change": "0.71",
  "balance": "0.00",
  "message": "Calculation successful"
}
```

Input must be integer or float.
And error message will be return if amount_provided is less than total_cost.

```javascript
"Amount provided must be greater than total cost"
```
